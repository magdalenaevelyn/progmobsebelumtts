package com.example.progmob_sebelumtts.Network;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Model.Dosen;
import com.example.progmob_sebelumtts.Model.LoginResponse;
import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.Model.Matakuliah;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface GetDataService {
    //Mendefinisikan semua API
    //nim_progmob = siapa yang membuat datanya
    //nim = nim dari data mhs yang akan diinsert
    //api/progmob/mhs = endpoint = url setelah base url (url utama dari website)
    //base URL ada di RetrofitClientInstance

    //LOGIN
    @FormUrlEncoded
    @POST("api/progmob/login")
    Call<List<LoginResponse>>login(
            @Field("nimnik") String nimnik,
            @Field("password") String password
    );

    //MAHASISWA
    @GET("api/progmob/mhs/{nim_progmob}")
    Call<List<Mahasiswa>> getMahasiswa(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/mhs/delete")
    Call<DefaultResult>delete_mhs(
            @Field("nim") String id,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/mhs/create")
    Call<DefaultResult> add_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/mhs/update")
    Call<DefaultResult>update_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("nim_cari") String nim_cari,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //DOSEN
    @GET("api/progmob/dosen/{nim_progmob}")
    Call<List<Dosen>> getDosen(
            @Path("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/dosen/delete")
    Call<DefaultResult>delete_dosen(
            @Field("nidn") String nidn,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/dosen/create")
    Call<DefaultResult> add_dosen(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/dosen/update")
    Call<DefaultResult>update_dosen(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob,
            @Field("nidn_cari") String nidn_cari
    );

    //MATAKULIAH
    @GET("api/progmob/matkul/{nim_progmob}")
    Call<List<Matakuliah>> getMatkul(
            @Path("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/delete")
    Call<DefaultResult>delete_matkul(
            @Field("kode") String kode,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/create")
    Call<DefaultResult> add_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") Integer hari,
            @Field("sesi") Integer sesi,
            @Field("sks") Integer sks
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/update")
    Call<DefaultResult>update_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") Integer hari,
            @Field("sesi") Integer sesi,
            @Field("sks") Integer sks,
            @Field("kode_cari") String kode_cari
    );
}