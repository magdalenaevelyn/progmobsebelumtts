package com.example.progmob_sebelumtts.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob_sebelumtts.Adapter.MahasiswaCardAdapter;
import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);

//        CardView cvMahasiswa = (CardView)findViewById(R.id.cvMahasiswa);
        RecyclerView cvMahasiswa = (RecyclerView)findViewById(R.id.cvMahasiswa);
        MahasiswaCardAdapter mahasiswaCardAdapter;

        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();
        Mahasiswa m1 = new Mahasiswa("Evi", "72180180", "0202020202");
        Mahasiswa m2 = new Mahasiswa("Felix", "72180190", "0892929299");
        Mahasiswa m3 = new Mahasiswa("Elbie", "72180196", "01010101101");
        Mahasiswa m4 = new Mahasiswa("Nadia", "72180200", "999229001");
        Mahasiswa m5 = new Mahasiswa("Argo", "72180100", "2992929229");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(CardViewTestActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        cvMahasiswa.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
        cvMahasiswa.setAdapter(mahasiswaCardAdapter);
    }
}