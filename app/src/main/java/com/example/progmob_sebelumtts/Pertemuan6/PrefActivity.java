package com.example.progmob_sebelumtts.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob_sebelumtts.R;

public class PrefActivity extends AppCompatActivity {

    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref = (Button)findViewById(R.id.btnPref);
        SharedPreferences pref = PrefActivity.this.getSharedPreferences("pref_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        //membaca pref isLogin apakah true atau false
        //isLogin -> keynya buat dipanggil
        //angka 0 -> kalau null responnya apa
        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")){
            //login
            btnPref.setText("Logout");
        } else {
            //not login
            btnPref.setText("Login");
        }

        //pengisian pref
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //cek preference terbaru seperti apa
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    //login
                    editor.putString("isLogin", "1");
                    btnPref.setText("Logout");
                } else {
                    //belum login
                    editor.putString("isLogin", "0");
                    btnPref.setText("Login");
                }

                //menyimpan perubahandi preferences
                editor.commit();

            }
        });
    }
}