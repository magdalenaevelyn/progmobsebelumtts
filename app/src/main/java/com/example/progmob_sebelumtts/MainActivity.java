package com.example.progmob_sebelumtts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.progmob_sebelumtts.Pertemuan2.CardViewTestActivity;
import com.example.progmob_sebelumtts.Pertemuan2.ListActivity;
import com.example.progmob_sebelumtts.Pertemuan2.RecyclerActivity;
import com.example.progmob_sebelumtts.Pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("abc", "test");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        setContentView(R.layout.tes_constraint_layout);
//        setContentView(R.layout.table_layout);

        if(savedInstanceState != null) {
            Log.d("Coba Recreated Activity", savedInstanceState.getString("abc"));
        }

        //VARIABLE
        TextView tvNama = findViewById(R.id.tvNama);
        final TextView txtView = (TextView)findViewById(R.id.mainActivityTextView);
        Button myBtn = (Button)findViewById(R.id.button1);
        final EditText myEditText = (EditText)findViewById(R.id.ediText1);
        Button btnHelp = (Button)findViewById(R.id.btnHelp);
        Button btnTracker = (Button)findViewById(R.id.btnTracker);

        //PERTEMUAN 2
        Button btnListView = (Button)findViewById(R.id.btnListView);
        Button btnRecyclerView = (Button)findViewById(R.id.btnRecyclerView);
        Button btnCardView = (Button)findViewById(R.id.btnCardView);

        //PERTEMUAN 4
        Button btnDebug = (Button)findViewById((R.id.btnDebug));

        //ACTION
        tvNama.setText("Magdalena Evelyn Halim");
        txtView.setText(R.string.konstanta);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.d("COBA KLICKKKKKKKKKKKK", myEditText.getText().toString());
                txtView.setText(myEditText.getText().toString());
            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                Bundle b = new Bundle();

                //kirim yang ada di edit text ke activity help dengan key help_string
                b.putString("help_string", myEditText.getText().toString());
                intent.putExtras(b);

                //menjalankan buttonnya yang mengarah ke HelpActivity
                startActivity(intent);
            }
        });
        btnTracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentTracker = new Intent(MainActivity.this, TrackerActivity.class);
                startActivity(intentTracker);
            }
        });

        //PERTEMUAN 2
        btnListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });
        btnRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intent);
            }
        });
        btnCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CardViewTestActivity.class);
                startActivity(intent);
            }
        });

        //PERTEMUAN 4
        btnDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(intent);
            }
        });
    }
}