package com.example.progmob_sebelumtts.Model;

public class MahasiswaDebugging {
    private String nama;
    private String nim;
    private String noTelp;

    public MahasiswaDebugging(String nama, String nim, String notelp) {
        this.nama = nama;
        this.nim = nim;
        this.noTelp = notelp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }
}
