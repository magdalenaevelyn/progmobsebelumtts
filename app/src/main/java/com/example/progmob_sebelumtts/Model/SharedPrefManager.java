package com.example.progmob_sebelumtts.Model;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    private static SharedPrefManager minst;
    private static Context mct;

    private static final String SHARD_PERFNAME= "pref_file";
    private static final String KEY_USERNAME= "nimnik";
    private static final String KEY_PASSWORD = "password";

    public SharedPrefManager(Context context){
        mct = context;
    }

    public static synchronized SharedPrefManager getInstans(Context context){
        if (minst==null){
            minst=new SharedPrefManager(context);
        }
        return minst;
    }

    //ngemasukin username & passwordnya ke shared preferences -> NYIMPEN SESSION
    public boolean userLogin(String name, String password){
        SharedPreferences sharedPreferences= mct.getSharedPreferences(SHARD_PERFNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USERNAME, name);
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
        return true;
    }

    //BUAT NGECEK ADA SESSION ATAU ENGGA
    public boolean isLogin(){
        SharedPreferences sharedPreferences= mct.getSharedPreferences(SHARD_PERFNAME, Context.MODE_PRIVATE);
        if (sharedPreferences.getString(KEY_USERNAME,null) !=null){
            return true;
        }
        return false;
    }

    //CLEAR SESSION BIAR BISA LOGOUT
    public boolean logout(){
        SharedPreferences sharedPreferences=mct.getSharedPreferences(SHARD_PERFNAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    //MENGAMBIL USERNAMENYA
    public String getUsername(){
        SharedPreferences sharedPreferences=mct.getSharedPreferences(SHARD_PERFNAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME,null);
    }

    //MENGAMBIL PASSWORDNYA
    public String getUserPassword(){
        SharedPreferences sharedPreferences=mct.getSharedPreferences(SHARD_PERFNAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PASSWORD,null);
    }

//    public String makeServiceCall(String reqUrl){
//        String respone = null;
//        try {
//            URL url = new URL(reqUrl);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("GET");
//        }
//    }
}

