package com.example.progmob_sebelumtts.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.Model.Matakuliah;
import com.example.progmob_sebelumtts.Pertemuan567_CRUD.UpdateMatkulActivity;
import com.example.progmob_sebelumtts.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MatakuliahCRUDRecyclerAdapter extends RecyclerView.Adapter<MatakuliahCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Matakuliah> matakuliahList;

    public MatakuliahCRUDRecyclerAdapter(Context context) {
        this.context = context;
        matakuliahList = new ArrayList<>();
    }

    public MatakuliahCRUDRecyclerAdapter(List<Matakuliah> matakuliahList) {
        this.matakuliahList = matakuliahList;
    }

    public List<Matakuliah> getMatakuliahListList() {
        return matakuliahList;
    }

    public void setMatakuliahListList(List<Matakuliah> matakuliahList) {
        this.matakuliahList = matakuliahList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MatakuliahCRUDRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MatakuliahCRUDRecyclerAdapter.ViewHolder holder, int position) {
        Matakuliah mtk = matakuliahList.get(position);
        holder.tvNamaMatkul.setText(mtk.getNamaMatkul());
        holder.tvKodeMatkul.setText(mtk.getKodeMatkul());
//        holder.tvHari.setText(mtk.getHari());
//        holder.tvSesi.setText(mtk.getSesi());
//        holder.tvSks.setText(mtk.getSks());
        holder.tvHari.setText(String.valueOf(mtk.getHari()));
        holder.tvSesi.setText(String.valueOf(mtk.getSesi()));
        holder.tvSks.setText(String.valueOf(mtk.getSks()));
        holder.mtk = mtk;
    }

    @Override
    public int getItemCount() {
        return matakuliahList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaMatkul, tvKodeMatkul, tvHari, tvSesi, tvSks;
        private RecyclerView rvMatkulTts;
        private ImageView imgMatkul;
        Matakuliah mtk;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNamaMatkul = itemView.findViewById(R.id.tvNamaMatkul);
            tvKodeMatkul = itemView.findViewById(R.id.tvKodeMatkul);
            tvHari = itemView.findViewById(R.id.tvHariMatkul);
            tvSesi = itemView.findViewById(R.id.tvSesiMatkul);
            tvSks = itemView.findViewById(R.id.tvSksMatkul);
            rvMatkulTts = itemView.findViewById(R.id.rvMatkulTts);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentUpdateMatkul = new Intent(itemView.getContext(), UpdateMatkulActivity.class);
                    intentUpdateMatkul.putExtra("nama", mtk.getNamaMatkul());
                    intentUpdateMatkul.putExtra("kode", mtk.getKodeMatkul());
//                    intentUpdateMatkul.putExtra("hari", mtk.getHari());
//                    intentUpdateMatkul.putExtra("sesi", mtk.getSesi());
//                    intentUpdateMatkul.putExtra("sks", mtk.getSks());
                    intentUpdateMatkul.putExtra("hari", String.valueOf(mtk.getHari()));
                    intentUpdateMatkul.putExtra("sesi", String.valueOf(mtk.getSesi()));
                    intentUpdateMatkul.putExtra("sks", String.valueOf(mtk.getSks()));

                    itemView.getContext().startActivity(intentUpdateMatkul);
                }
            });
        }
    }
}
