package com.example.progmob_sebelumtts.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob_sebelumtts.Model.Dosen;
import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.Pertemuan567_CRUD.UpdateDosenActivity;
import com.example.progmob_sebelumtts.Pertemuan567_CRUD.UpdateMhsActivity;
import com.example.progmob_sebelumtts.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DosenCRUDRecyclerAdapter extends RecyclerView.Adapter<DosenCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public DosenCRUDRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenCRUDRecyclerAdapter(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_dosen, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen dsn = dosenList.get(position);
        holder.tvNamaDosen.setText(dsn.getNama());
        holder.tvNidnDosen.setText(dsn.getNidn());
        holder.tvAlamatDosen.setText(dsn.getAlamat());
        holder.tvEmailDosen.setText(dsn.getEmail());
        holder.tvGelarDosen.setText(dsn.getGelar());
        holder.imgDosen.getLayoutParams().height = 150;
        holder.imgDosen.getLayoutParams().width = 150;
        if(dosenList.get(position).getFoto() != null){
            Picasso.get()
                    .load(dsn.getFoto())
                    .into(holder.imgDosen);
        }
        holder.dosen = dsn;
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaDosen, tvNidnDosen, tvAlamatDosen, tvEmailDosen, tvGelarDosen;
        private RecyclerView rvGetDosen;
        private ImageView imgDosen;
        Dosen dosen;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNamaDosen = itemView.findViewById(R.id.tvNamaDosen);
            tvNidnDosen = itemView.findViewById(R.id.tvNidnDosen);
            tvAlamatDosen = itemView.findViewById(R.id.tvAlamatDosen);
            tvEmailDosen = itemView.findViewById(R.id.tvEmailDosen);
            tvGelarDosen = itemView.findViewById(R.id.tvGelarDosen);
            imgDosen = itemView.findViewById(R.id.imgDosen);
            rvGetDosen = itemView.findViewById(R.id.rvDosenTts);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent goInput = new Intent(itemView.getContext(), UpdateDosenActivity.class);
                    goInput.putExtra("nidn", dosen.getNidn());
                    goInput.putExtra("nama", dosen.getNama());
                    goInput.putExtra("alamat", dosen.getAlamat());
                    goInput.putExtra("email", dosen.getEmail());
                    goInput.putExtra("gelar", dosen.getGelar());

                    itemView.getContext().startActivity(goInput);
                }
            });
        }
    }
}
