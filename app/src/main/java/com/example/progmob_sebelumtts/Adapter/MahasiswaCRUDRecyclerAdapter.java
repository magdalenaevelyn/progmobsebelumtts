package com.example.progmob_sebelumtts.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.Pertemuan567_CRUD.MahasiswaUpdateActivity;
import com.example.progmob_sebelumtts.Pertemuan567_CRUD.UpdateMhsActivity;
import com.example.progmob_sebelumtts.R;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MahasiswaCRUDRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_mhs, parent, false);
        return new MahasiswaCRUDRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaCRUDRecyclerAdapter.ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);
        holder.tvNama.setText(m.getNama());
        holder.tvNim.setText(m.getNim());
//        holder.tvNoTelp.setText(m.getNoTelp());
        holder.tvAlamat.setText(m.getAlamat());
        holder.tvEmail.setText(m.getEmail());
        holder.imgMhs.getLayoutParams().height = 150;
        holder.imgMhs.getLayoutParams().width = 150;
        if(mahasiswaList.get(position).getFoto() != null){
            Picasso.get()
                    .load(m.getFoto())
                    .into(holder.imgMhs);
        }
        holder.mhs = m;
    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNim, tvNoTelp, tvAlamat, tvEmail;
        private ImageView imgMhs;
        private RecyclerView rvGetMhsAll;
        Mahasiswa mhs;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNamaMhs);
            tvNim = itemView.findViewById(R.id.tvNimMhs);
            tvAlamat = itemView.findViewById(R.id.tvAlamatMhs);
            tvEmail = itemView.findViewById(R.id.tvEmailMhs);
            imgMhs = itemView.findViewById(R.id.imgMhs);
            rvGetMhsAll = itemView.findViewById(R.id.rvMhsTts);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent goInput = new Intent(itemView.getContext(), UpdateMhsActivity.class);
                    goInput.putExtra("nim", mhs.getNim());
                    goInput.putExtra("nama", mhs.getNama());
                    goInput.putExtra("alamat", mhs.getAlamat());
                    goInput.putExtra("email", mhs.getEmail());

                    itemView.getContext().startActivity(goInput);
                }
            });
        }
    }
}
