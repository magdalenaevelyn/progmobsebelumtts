package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateMatkulActivity extends AppCompatActivity {

    EditText edtNamaMatkul, edtKodeMatkul, edtHariMatkul, edtSesiMatkul, edtSksMatkul;
    Button btnTambahMatkul;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_matkul);

        edtNamaMatkul = (EditText)findViewById(R.id.edtNamaMatkul);
        edtKodeMatkul = (EditText)findViewById(R.id.edtKodeMatkul);
        edtHariMatkul = (EditText)findViewById(R.id.edtHariMatkul);
        edtSesiMatkul = (EditText)findViewById(R.id.edtSesiMatkul);
        edtSksMatkul = (EditText)findViewById(R.id.edtSksMatkul);
        btnTambahMatkul = (Button) findViewById(R.id.btnTambahMatkul);
        Toolbar toolbar = (Toolbar)findViewById(R.id.tbCreateMatkul);
        pd = new ProgressDialog(CreateMatkulActivity.this);

        btnTambahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                Integer edtHari = Integer.parseInt(edtHariMatkul.getText().toString());
                Integer edtSesi = Integer.parseInt(edtSesiMatkul.getText().toString());
                Integer edtSks = Integer.parseInt(edtSksMatkul.getText().toString());

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        edtNamaMatkul.getText().toString(),
                        "72180180",
                        edtKodeMatkul.getText().toString(),
                        edtHari,
                        edtSesi,
                        edtSks
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(CreateMatkulActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(CreateMatkulActivity.this, ReadMatkulActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(CreateMatkulActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}