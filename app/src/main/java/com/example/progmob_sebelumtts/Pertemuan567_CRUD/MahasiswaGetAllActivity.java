package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Adapter.MahasiswaCRUDRecyclerAdapter;
import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaGetAllActivity extends AppCompatActivity {

    //Variable global karena akan dipanggil asynchronus
    RecyclerView rvMhs;
    MahasiswaCRUDRecyclerAdapter mhsAdapter;
    ProgressDialog pd; //menggambarkan aktivitas ketika ngambil data internet
    List<Mahasiswa> mahasiswaList; //akan diisi melalui internet

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_get_all);

        rvMhs = (RecyclerView)findViewById(R.id.rvGetMhsAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        //getdataservice manggil objek retrofit client
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        //Call dengan tipe returnnya list mahasiswa, parameternya nim_progmob
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180180");

        //atribut fungsi enqueue = diurutkan
        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MahasiswaGetAllActivity.this);
                rvMhs.setLayoutManager(layoutManager);
                rvMhs.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MahasiswaGetAllActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}