package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob_sebelumtts.Adapter.MahasiswaCRUDRecyclerAdapter;
import com.example.progmob_sebelumtts.MainActivity;
import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.Pertemuan4.DebuggingActivity;
import com.example.progmob_sebelumtts.R;

import java.util.List;

public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);

        Button buttonGetMhs = (Button)findViewById(R.id.buttonGetMhs);
        Button buttonAddMhs = (Button)findViewById(R.id.buttonAddMhs);
        Button buttonUpdateMhs = (Button)findViewById(R.id.buttonUpdateMhs);
        Button buttonHapusMhs = (Button)findViewById(R.id.buttonHapusMahasiswa);

        buttonGetMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGetAll = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                startActivity(intentGetAll);
            }
        });

        buttonAddMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentAdd = new Intent(MainMhsActivity.this, MahasiswaAddActivity.class);
                startActivity(intentAdd);
            }
        });

        buttonUpdateMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentUpdate = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                startActivity(intentUpdate);
            }
        });

        buttonHapusMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDelete = new Intent(MainMhsActivity.this, HapusMhsActivity.class);
                startActivity(intentDelete);
            }
        });
    }
}