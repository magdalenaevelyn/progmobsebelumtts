package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateDosenActivity extends AppCompatActivity {

    EditText edtNidnDosen, edtNamaDosen, edtAlamatDosen, edtEmailDosen, edtGelarDosen;
    Button btnTambahDosen;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_dosen);

        edtNidnDosen = (EditText)findViewById(R.id.edtNidnDosen);
        edtNamaDosen = (EditText)findViewById(R.id.edtNamaDosen);
        edtAlamatDosen = (EditText)findViewById(R.id.edtAlamatDosen);
        edtEmailDosen = (EditText)findViewById(R.id.edtEmailDosen);
        edtGelarDosen = (EditText)findViewById(R.id.edtGelarDosen);
        btnTambahDosen = (Button)findViewById(R.id.btnTambahDosen);
        androidx.appcompat.widget.Toolbar toolbar = (Toolbar)findViewById(R.id.tbCreateDosen);

        pd = new ProgressDialog(CreateDosenActivity.this);

        btnTambahDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        edtNamaDosen.getText().toString(),
                        edtNidnDosen.getText().toString(),
                        edtAlamatDosen.getText().toString(),
                        edtEmailDosen.getText().toString(),
                        edtGelarDosen.getText().toString(),
                        "foto dosen",
                        "72180180"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(CreateDosenActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(CreateDosenActivity.this, ReadDosenActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(CreateDosenActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}