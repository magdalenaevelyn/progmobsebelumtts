package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progmob_sebelumtts.R;
import com.google.android.material.snackbar.Snackbar;

public class MainMenuActivity extends AppCompatActivity {

    ImageView imgDafMhs, imgDafDosen, imgDafMatkul, imgDafJadwal;
    TextView tvDafMhs, tvDafDosen, tvDafMatkul, tvDafJadwal;
    LinearLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Toolbar toolbar = (Toolbar)findViewById(R.id.tbMainMenu);
        setSupportActionBar(toolbar); //untuk siap diisi menu

        imgDafMhs = (ImageView)findViewById(R.id.imgDafMhs);
        imgDafDosen = (ImageView)findViewById(R.id.imgDafDosen);
        imgDafMatkul = (ImageView)findViewById(R.id.imgDafMatkul);
        imgDafJadwal = (ImageView)findViewById(R.id.imgDafJadwal);

        tvDafMhs = (TextView)findViewById(R.id.tvDafMhs);
        tvDafDosen = (TextView)findViewById(R.id.tvDafDosen);
        tvDafMatkul = (TextView)findViewById(R.id.tvDafMatkul);
        tvDafJadwal = (TextView)findViewById(R.id.tvDafJadwal);

        imgDafMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMhs = new Intent(MainMenuActivity.this, ReadMhsActivity.class);
                startActivity(intentMhs);
            }
        });

        tvDafMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMhs = new Intent(MainMenuActivity.this, ReadMhsActivity.class);
                startActivity(intentMhs);
            }
        });

        imgDafDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDosen = new Intent(MainMenuActivity.this, ReadDosenActivity.class);
                startActivity(intentDosen);
            }
        });
        tvDafDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDosen = new Intent(MainMenuActivity.this, ReadDosenActivity.class);
                startActivity(intentDosen);
            }
        });

        imgDafMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMatkul = new Intent(MainMenuActivity.this, ReadMatkulActivity.class);
                startActivity(intentMatkul);
            }
        });

        tvDafMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMatkul = new Intent(MainMenuActivity.this, ReadMatkulActivity.class);
                startActivity(intentMatkul);
            }
        });
    }

    //MENU LOGOUT
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.logout:
//                currentLayout = (LinearLayout)findViewById(R.id.linearlayout);
//                Snackbar.make(currentLayout,"Logout",Snackbar.LENGTH_LONG).show();
//                Toast.makeText(this, "Anda berhasil Logout", Toast.LENGTH_LONG).show();
//                Intent logout = new Intent(MainMenuActivity.this, LoginActivity.class);
//                startActivity(logout);
//            default:
//                return super.onOptionsItemSelected(item);
//    }
        if(item.getItemId() == R.id.logout){
                AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
                builder.setMessage("Apakah anda yakin untuk logout?")
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(MainMenuActivity.this, "Batal Logout", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SharedPreferences pref = MainMenuActivity.this.getSharedPreferences("pref_file", MODE_PRIVATE);

                                String isLogin = pref.getString("isLogin", null);
                                SharedPreferences.Editor edit = pref.edit();
                                edit.putString("isLogin", "0");
//                                edit.remove("nimnik");
//                                edit.remove("password");
//                                edit.clear();
                                edit.commit();

                                Intent intentLogout = new Intent(MainMenuActivity.this, LoginActivity.class);
                                intentLogout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentLogout);
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return true;
    }
}
