package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Adapter.MatakuliahCRUDRecyclerAdapter;
import com.example.progmob_sebelumtts.Model.Matakuliah;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReadMatkulActivity extends AppCompatActivity {

    RecyclerView rvMatkulTts;
    MatakuliahCRUDRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<Matakuliah> matakuliahList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_matkul);

        rvMatkulTts = (RecyclerView)findViewById(R.id.rvMatkulTts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        Toolbar toolbar = (Toolbar)findViewById(R.id.tbReadMatkul);
        setSupportActionBar(toolbar); //untuk siap diisi menu

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matakuliah>> call = service.getMatkul(
                "72180180"
        );
        call.enqueue(new Callback<List<Matakuliah>>() {
            @Override
            public void onResponse(Call<List<Matakuliah>> call, Response<List<Matakuliah>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                matkulAdapter = new MatakuliahCRUDRecyclerAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ReadMatkulActivity.this);
                rvMatkulTts.setLayoutManager(layoutManager);
                rvMatkulTts.setAdapter(matkulAdapter);
            }

            @Override
            public void onFailure(Call<List<Matakuliah>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ReadMatkulActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_matkul, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.createMatkul:
                Intent intentCreateMatkul = new Intent(ReadMatkulActivity.this, CreateMatkulActivity.class);
                startActivity(intentCreateMatkul);
                finish();
                return true;
            case R.id.deleteMatkul:
                Intent intentDeleteMatkul = new Intent(ReadMatkulActivity.this, DeleteMatkulActivity.class);
                startActivity(intentDeleteMatkul);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}