package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaAddActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_add);

        EditText editTextNama = (EditText)findViewById(R.id.editTextNama);
        EditText editTextNim = (EditText)findViewById(R.id.editTextNim);
        EditText editTextAlamat = (EditText)findViewById(R.id.editTextAlamat);
        EditText editTextEmail = (EditText)findViewById(R.id.editTextEmail);
        Button btnSimpan = (Button)findViewById(R.id.buttonSimpanMhs);
        pd = new ProgressDialog(MahasiswaAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                      editTextNama.getText().toString(),
                      editTextNim.getText().toString(),
                      editTextAlamat.getText().toString(),
                      editTextEmail.getText().toString(),
                      "kosongkan saja diisi sembarang karena dirandom sistem",
                      "72180180"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}