package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.LoginResponse;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity{

    EditText edtNimLogin, edtPassLogin;
    Button btnLogin;
    String isLogin = "";
    private static final String PREFER_NAME = "pref_file";
    List<LoginResponse> loginResponseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtNimLogin = (EditText) findViewById(R.id.edtNimLogin);
        edtPassLogin = (EditText) findViewById(R.id.edtPassLogin);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        SharedPreferences sharedPreferences = getSharedPreferences(PREFER_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        isLogin = sharedPreferences.getString("isLogin", "0");
        if (isLogin.equals("1")) {
            Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
            startActivity(intent);
        } else {
            //not login
            btnLogin.setText("Login");
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<LoginResponse>> call = service.login(
                        edtNimLogin.getText().toString(),
                        edtPassLogin.getText().toString()
                );

                if (edtNimLogin.getText().toString().isEmpty() && edtPassLogin.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Wajib memasukkan NIM & Password", Toast.LENGTH_LONG).show();
                    return;
                }

                call.enqueue(new Callback<List<LoginResponse>>() {
                    @Override
                    public void onResponse(Call<List<LoginResponse>> call, Response<List<LoginResponse>> response) {
                        loginResponseList = response.body();
                        if (response.isSuccessful()) {
                            if (loginResponseList.size() == 0) {
                                Toast.makeText(LoginActivity.this, "Silahkan memasukkan valid username & password", Toast.LENGTH_LONG).show();
                            } else {
                                editor.putString("nimnik", loginResponseList.get(0).getNimnik().toString());
                                editor.putString("password", loginResponseList.get(0).getPassword().toString());
                                editor.putString("isLogin", "1");
                                editor.commit();
                                Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
//                                finish();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<LoginResponse>> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}