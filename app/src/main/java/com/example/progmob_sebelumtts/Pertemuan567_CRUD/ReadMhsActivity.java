package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Adapter.MahasiswaCRUDRecyclerAdapter;
import com.example.progmob_sebelumtts.Model.Mahasiswa;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReadMhsActivity extends AppCompatActivity {

    RecyclerView rvMhsTts;
    MahasiswaCRUDRecyclerAdapter mhsAdapter;
    ProgressDialog pd;
    List<Mahasiswa> mahasiswaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_mhs);

        rvMhsTts = (RecyclerView)findViewById(R.id.rvMhsTts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        Toolbar toolbar = (Toolbar)findViewById(R.id.tbReadMhs);
        setSupportActionBar(toolbar); //untuk siap diisi menu

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180180");
        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ReadMhsActivity.this);
                rvMhsTts.setLayoutManager(layoutManager);
                rvMhsTts.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ReadMhsActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.create:
                Intent intentCreateMhs = new Intent(ReadMhsActivity.this, CreateMhsActivity.class);
                startActivity(intentCreateMhs);
                finish();
                return true;
            case R.id.delete:
                Intent intentDeleteMhs = new Intent(ReadMhsActivity.this, DeleteMhsActivity.class);
                startActivity(intentDeleteMhs);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}