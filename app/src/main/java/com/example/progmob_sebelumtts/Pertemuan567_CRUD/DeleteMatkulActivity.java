package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteMatkulActivity extends AppCompatActivity {

    EditText edtKodeMatkulHapus;
    Button btnDeleteMatkul;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_matkul);

        edtKodeMatkulHapus = (EditText)findViewById(R.id.edtKodeMatkulHapus);
        btnDeleteMatkul = (Button)findViewById(R.id.btnDeleteMatkul);
        pd = new ProgressDialog(DeleteMatkulActivity.this);
        androidx.appcompat.widget.Toolbar toolbar = (Toolbar)findViewById(R.id.tbDeleteMatkul);

        btnDeleteMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_matkul(
                        edtKodeMatkulHapus.getText().toString(),
                        "72180180"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DeleteMatkulActivity.this, "DATA BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DeleteMatkulActivity.this, ReadMatkulActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DeleteMatkulActivity.this, "DATA GAGAL DIHAPUS", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}