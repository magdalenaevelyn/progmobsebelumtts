package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMatkulActivity extends AppCompatActivity {

    EditText edtKodeMatkulCari, edtNamaMatkulBaru, edtKodeMatkulBaru, edtHariMatkulBaru, edtSesiMatkulBaru, edtSksMatkulBaru;
    Button btnUpdateMatkul;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_matkul);

        edtKodeMatkulCari = (EditText)findViewById(R.id.edtKodeMatkulCari);
        edtNamaMatkulBaru = (EditText)findViewById(R.id.edtNamaMatkulBaru);
        edtKodeMatkulBaru = (EditText)findViewById(R.id.edtKodeMatkulBaru);
        edtHariMatkulBaru = (EditText)findViewById(R.id.edtHariMatkulBaru);
        edtSesiMatkulBaru = (EditText)findViewById(R.id.edtSesiMatkulBaru);
        edtSksMatkulBaru = (EditText)findViewById(R.id.edtSksMatkulBaru);
        btnUpdateMatkul = (Button)findViewById(R.id.btnUpdateMatkul);
        pd = new ProgressDialog(UpdateMatkulActivity.this);

        Intent data = getIntent();

        edtKodeMatkulCari.setText(data.getStringExtra("kode"));
        edtNamaMatkulBaru.setText(data.getStringExtra("nama"));
        edtKodeMatkulBaru.setText(data.getStringExtra("kode"));
        edtHariMatkulBaru.setText(data.getStringExtra("hari"));
        edtSesiMatkulBaru.setText(data.getStringExtra("sesi"));
        edtSksMatkulBaru.setText(data.getStringExtra("sks"));

        btnUpdateMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                Integer edtHariBaru = Integer.parseInt(edtHariMatkulBaru.getText().toString());
                Integer edtSesiBaru = Integer.parseInt(edtSesiMatkulBaru.getText().toString());
                Integer edtSksBaru = Integer.parseInt(edtSksMatkulBaru.getText().toString());
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        edtNamaMatkulBaru.getText().toString(),
                        "72180180",
                        edtKodeMatkulBaru.getText().toString(),
                        edtHariBaru,
                        edtSesiBaru,
                        edtSksBaru,
                        edtKodeMatkulCari.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateMatkulActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(UpdateMatkulActivity.this, ReadMatkulActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMatkulActivity.this, "DATA GAGAL DIUBAH", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}