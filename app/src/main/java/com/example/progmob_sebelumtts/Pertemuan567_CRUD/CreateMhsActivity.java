package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateMhsActivity extends AppCompatActivity {

    EditText edtNimMhs, edtNamaMhs, edtAlamatMhs, edtEmailMhs;
    Button btnTambah;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_mhs);

        edtNimMhs = (EditText)findViewById(R.id.edtNimMhs);
        edtNamaMhs = (EditText)findViewById(R.id.edtNamaMhs);
        edtAlamatMhs = (EditText)findViewById(R.id.edtAlamatMhs);
        edtEmailMhs = (EditText)findViewById(R.id.edtEmailMhs);
        btnTambah = (Button)findViewById(R.id.btnTambahMhs);
        androidx.appcompat.widget.Toolbar toolbar = (Toolbar)findViewById(R.id.tbCreateMhs);

        pd = new ProgressDialog(CreateMhsActivity.this);

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edtNamaMhs.getText().toString(),
                        edtNimMhs.getText().toString(),
                        edtAlamatMhs.getText().toString(),
                        edtEmailMhs.getText().toString(),
                        "kosongkan saja diisi sembarang karena dirandom sistem",
                        "72180180"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(CreateMhsActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(CreateMhsActivity.this, ReadMhsActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(CreateMhsActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}