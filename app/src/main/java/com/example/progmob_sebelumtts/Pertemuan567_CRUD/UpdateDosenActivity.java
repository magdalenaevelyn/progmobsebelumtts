package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateDosenActivity extends AppCompatActivity {

    EditText edtNidnDosenCari, edtNamaDosenBaru, edtNidnDosenBaru, edtAlamatDosenBaru, edtEmailDosenBaru, edtGelarDosenBaru;
    Button btnUpdateDosen;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_dosen);

        edtNidnDosenCari = (EditText)findViewById(R.id.edtNidnDosenCari);
        edtNamaDosenBaru = (EditText)findViewById(R.id.edtNamaDosenBaru);
        edtNidnDosenBaru = (EditText)findViewById(R.id.edtNidnDosenBaru);
        edtAlamatDosenBaru = (EditText)findViewById(R.id.edtAlamatDosenBaru);
        edtEmailDosenBaru = (EditText)findViewById(R.id.edtEmailDosenBaru);
        edtGelarDosenBaru = (EditText)findViewById(R.id.edtGelarDosenBaru);
        btnUpdateDosen = (Button)findViewById(R.id.btnUpdateDosen);
        pd = new ProgressDialog(UpdateDosenActivity.this);

        Intent data = getIntent();
        edtNidnDosenCari.setText(data.getStringExtra("nidn"));
        edtNamaDosenBaru.setText(data.getStringExtra("nama"));
        edtNidnDosenBaru.setText(data.getStringExtra("nidn"));
        edtAlamatDosenBaru.setText(data.getStringExtra("alamat"));
        edtEmailDosenBaru.setText(data.getStringExtra("email"));
        edtGelarDosenBaru.setText(data.getStringExtra("gelar"));

        btnUpdateDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dosen(
                        edtNamaDosenBaru.getText().toString(),
                        edtNidnDosenBaru.getText().toString(),
                        edtAlamatDosenBaru.getText().toString(),
                        edtEmailDosenBaru.getText().toString(),
                        edtGelarDosenBaru.getText().toString(),
                        "Foto ini diubah ya",
                        "72180180",
                        edtNidnDosenCari.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(UpdateDosenActivity.this, ReadDosenActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "DATA GAGAL DIUBAH", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}