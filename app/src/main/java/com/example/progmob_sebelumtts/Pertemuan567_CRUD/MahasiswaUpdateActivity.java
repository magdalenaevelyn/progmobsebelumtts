package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {

    EditText editTextNimCari, editTextNamaBaru, editTextNimBaru, editTextAlamatBaru, editTextEmailBaru;
    Button btnUbah;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        editTextNimCari = (EditText)findViewById(R.id.editTextNimCari);
        editTextNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        editTextNimBaru = (EditText)findViewById(R.id.editTextNimBaru);
        editTextAlamatBaru = (EditText)findViewById(R.id.editTextAlamatBaru);
        editTextEmailBaru = (EditText)findViewById(R.id.editTextEmailBaru);
        btnUbah = (Button)findViewById(R.id.buttonUbah);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        Intent data = getIntent();
        editTextNimCari.setText(data.getStringExtra("nim"));
        editTextNamaBaru.setText(data.getStringExtra("nama"));
        editTextNimBaru.setText(data.getStringExtra("nim"));
        editTextAlamatBaru.setText(data.getStringExtra("alamat"));
        editTextEmailBaru.setText(data.getStringExtra("email"));

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

//                String name = editTextNamaBaru.getText().toString();
//                String nimm = editTextNimBaru.getText().toString();
//                String nimcari = editTextNimCari.getText().toString();
//                String address = editTextAlamatBaru.getText().toString();
//                String email = editTextEmailBaru.getText().toString();
//                String word = "Nama: "+ name +" NIM: "+ nimm + "nimcari " + nimcari +"Alamat: " + address +"Email: " + email;
//                Log.e("TAG", word);

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        editTextNamaBaru.getText().toString(),
                        editTextNimBaru.getText().toString(),
                        editTextNimCari.getText().toString(),
                        editTextAlamatBaru.getText().toString(),
                        editTextEmailBaru.getText().toString(),
                        "Foto ini diubah ya",
                        "72180180"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "DATA GAGAL DIUBAH", Toast.LENGTH_LONG).show();
                    }
                });

                Intent intentBalikMainMhs = new Intent(MahasiswaUpdateActivity.this, MainMhsActivity.class);
                startActivity(intentBalikMainMhs);
            }
        });
    }
}