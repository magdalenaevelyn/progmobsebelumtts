package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Adapter.DosenCRUDRecyclerAdapter;
import com.example.progmob_sebelumtts.Model.Dosen;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReadDosenActivity extends AppCompatActivity {

    RecyclerView rvDsnTts;
    DosenCRUDRecyclerAdapter dosenAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_dosen);

        rvDsnTts = (RecyclerView)findViewById(R.id.rvDosenTts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        Toolbar toolbar = (Toolbar)findViewById(R.id.tbReadDosen);
        setSupportActionBar(toolbar);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180180");
        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dosenAdapter = new DosenCRUDRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ReadDosenActivity.this);
                rvDsnTts.setLayoutManager(layoutManager);
                rvDsnTts.setAdapter(dosenAdapter);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ReadDosenActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_dosen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.create:
                Intent intentCreateDosen = new Intent(ReadDosenActivity.this, CreateDosenActivity.class);
                startActivity(intentCreateDosen);
                finish();
                return true;
            case R.id.delete:
                Intent intentDeleteDosen = new Intent(ReadDosenActivity.this, DeleteDosenActivity.class);
                startActivity(intentDeleteDosen);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}