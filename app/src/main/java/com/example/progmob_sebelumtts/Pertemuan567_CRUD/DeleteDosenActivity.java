package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteDosenActivity extends AppCompatActivity {

    EditText edtNidnDosenHapus;
    Button btnDeleteDosen;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_dosen);

        edtNidnDosenHapus = (EditText)findViewById(R.id.edtNidnDosenHapus);
        btnDeleteDosen = (Button)findViewById(R.id.btnDeleteDosen);
        pd = new ProgressDialog(DeleteDosenActivity.this);
        androidx.appcompat.widget.Toolbar toolbar = (Toolbar)findViewById(R.id.tbDeleteDosen);

        btnDeleteDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_dosen(
                        edtNidnDosenHapus.getText().toString(),
                        "72180180"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DeleteDosenActivity.this, "DATA BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DeleteDosenActivity.this, ReadDosenActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DeleteDosenActivity.this, "DATA GAGAL DIHAPUS", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}