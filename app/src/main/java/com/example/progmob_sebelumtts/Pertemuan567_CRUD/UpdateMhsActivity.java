package com.example.progmob_sebelumtts.Pertemuan567_CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_sebelumtts.Model.DefaultResult;
import com.example.progmob_sebelumtts.Network.GetDataService;
import com.example.progmob_sebelumtts.Network.RetrofitClientInstance;
import com.example.progmob_sebelumtts.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMhsActivity extends AppCompatActivity {

    EditText edtNimMhsCari, edtNamaMhsBaru, edtNimMhsBaru, edtAlamatMhsBaru, edtEmailMhsBaru;
    Button btnUpdateMhs;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mhs);

        edtNimMhsCari = (EditText)findViewById(R.id.edtNimMhsCari);
        edtNamaMhsBaru = (EditText)findViewById(R.id.edtNamaMhsBaru);
        edtNimMhsBaru = (EditText)findViewById(R.id.edtNimMhsBaru);
        edtAlamatMhsBaru = (EditText)findViewById(R.id.edtAlamatMhsBaru);
        edtEmailMhsBaru = (EditText)findViewById(R.id.edtEmailMhsBaru);
        btnUpdateMhs = (Button)findViewById(R.id.btnUpdateMhs);
        pd = new ProgressDialog(UpdateMhsActivity.this);
        androidx.appcompat.widget.Toolbar toolbar = (Toolbar)findViewById(R.id.tbUpdateMhs);

        Intent data = getIntent();
        edtNimMhsCari.setText(data.getStringExtra("nim"));
        edtNamaMhsBaru.setText(data.getStringExtra("nama"));
        edtNimMhsBaru.setText(data.getStringExtra("nim"));
        edtAlamatMhsBaru.setText(data.getStringExtra("alamat"));
        edtEmailMhsBaru.setText(data.getStringExtra("email"));

        btnUpdateMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        edtNamaMhsBaru.getText().toString(),
                        edtNimMhsBaru.getText().toString(),
                        edtNimMhsCari.getText().toString(),
                        edtAlamatMhsBaru.getText().toString(),
                        edtEmailMhsBaru.getText().toString(),
                        "Foto ini diubah ya",
                        "72180180"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this, "DATA BERHASIL DIUBAH", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(UpdateMhsActivity.this, ReadMhsActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this, "DATA GAGAL DIUBAH", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}